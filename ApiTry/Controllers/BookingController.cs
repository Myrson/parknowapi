﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataAccess;

namespace ApiTry.Controllers
{
    public class BookingController : ApiController
    {
        public IEnumerable<Booking> Get()
        {
            using (parknowdatabaseEntities entities = new parknowdatabaseEntities())
            {
                return entities.Booking.ToList();
            }

        }
    }
}
