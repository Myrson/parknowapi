﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataAccess;


namespace ApiTry.Controllers
{
  
    public class UserController : ApiController
    {
        public IEnumerable<User> GetUsers()
        {
            parknowdatabaseEntities entities = new parknowdatabaseEntities();
             return  entities.User.ToList(); 
        }


        public User GetUser(int id)
        {
            using (parknowdatabaseEntities entities = new parknowdatabaseEntities())
            {
                return entities.User.FirstOrDefault(e => e.id == id);
            }
        }


       // [HttpGet]
       // [ActionName("Katamaran")]
        // Not an action method.
        //[NonAction]
    }
}
